<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <spring:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
    <spring:url value="/resources/css/jumbotron-narrow.css" var="jumbotronNarrowCss"/>
    <spring:url value="/resources/js/jquery-1.11.3.min.js" var="jqueryJs"/>
    <spring:url value="/resources/js/jquery-dateFormat.min.js" var="jqueryDateFormatJs"/>
    <spring:url value="/resources/js/bootstrap.min.js" var="bootstrapJs"/>
    <spring:url value="/resources/images/sky_small.png" var="skyLogoSmall"/>

    <title>Your Sky Bill</title>

    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link href="${jumbotronNarrowCss}" rel="stylesheet"/>
    <script src="${jqueryJs}"></script>
    <script src="${jqueryDateFormatJs}"></script>
    <script src="${bootstrapJs}"></script>

</head>
<body>
<div class="container">
    <div class="header clearfix">
        <a href="http://www.sky.com">
            <img src="${skyLogoSmall}" alt="Sky Home Page">
        </a>
    </div>

    <div class="jumbotron">
        <h1>Hi Anthony</h1>

        <p>Your bill was generated on: <strong>${currentBill.statement.generatedDate}</strong><br></p>
            <p>This bill is for the period: <br><strong>${currentBill.statement.billingPeriod.fromDate}</strong> to
            <strong>${currentBill.statement.billingPeriod.toDate}</strong></p>

        <p>The total for this month comes to: <strong>&pound;${currentBill.total}</strong></p>

        <p>Your next payment is due on: <strong>${currentBill.statement.dueDate}.</strong></p>
    </div>

    <div class="row marketing">
        <div class="col-lg-12" id="packageCharges">
            <h3>My Package</h3>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Cost</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="subscription" items="${currentBill.skyPackageCharges.charges}">
                <tr>
                    <td>${subscription.subscriptionType}</td>
                    <td>${subscription.description}</td>
                    <td>&pound;${subscription.cost}</td>
                </tr>
                </c:forEach>
            </table>
            <h4>Total Package Charges: &pound;${currentBill.skyPackageCharges.totalCost}</h4>
        </div>
    </div>

    <div class="row marketing">
        <div class="col-lg-12" id="callCharges">
            <h3>Call Charges</h3>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Duration</th>
                    <th>Cost</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="call" items="${currentBill.callCharges.charges}">
                <tr>
                    <td>${call.numberCalled}</td>
                    <td>${call.callDuration}</td>
                    <td>&pound;${call.cost}</td>
                </tr>
                </c:forEach>
            </table>
            <h4>Total Call Charges: &pound;${currentBill.callCharges.totalCost}</h4>
        </div>
    </div>

    <div class="row marketing" id="skyStoreCharges">
        <h3>Sky Store </h3>

        <div class="col-lg-6">
            <h4>Rentals</h4>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Cost</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="rental" items="${currentBill.skyStoreCharges.rentals}">
                    <tr>
                        <td>${rental.title}</td>
                        <td>&pound;${rental.cost}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="col-lg-6">
            <h4>Buy and Keep Purchases</h4>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Cost</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="buyAndKeep" items="${currentBill.skyStoreCharges.buyAndKeep}">
                    <tr>
                        <td>${buyAndKeep.title}</td>
                        <td>&pound;${buyAndKeep.cost}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <h4>Total Sky Store Charges: &pound;${currentBill.skyStoreCharges.totalCost}</h4>
    </div>

    <footer class="footer">
        <div>
                <ul class="nav nav-pills pull-right">
                    <li class="presentation"><a href="http://help.sky.com">Help</a></li>
                    <li class="presentation"><a href="http://contactus.sky.com">Contact</a></li>
                </ul>
        </div>
        <p>&copy; Sky 2015</p>
    </footer>

</div>
</body>
</html>