<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <spring:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
    <spring:url value="/resources/css/jumbotron-narrow.css" var="jumbotronNarrowCss"/>
    <spring:url value="/resources/js/jquery-1.11.3.min.js" var="jqueryJs"/>
    <spring:url value="/resources/js/jquery-dateFormat.min.js" var="jqueryDateFormatJs"/>
    <spring:url value="/resources/js/bootstrap.min.js" var="bootstrapJs"/>
    <spring:url value="/resources/images/sky_small.png" var="skyLogoSmall"/>

    <title>404 Page not found</title>

    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link href="${jumbotronNarrowCss}" rel="stylesheet"/>
    <script src="${jqueryJs}"></script>
    <script src="${jqueryDateFormatJs}"></script>
    <script src="${bootstrapJs}"></script>

</head>
<body>

<div class="container">
    <div class="header clearfix">
        <a href="http://www.sky.com">
            <img src="${skyLogoSmall}" alt="Sky Home Page">
        </a>
    </div>

    <div class="jumbotron">
        <h1>An error has occurred!</h1>

        <p>Please try again later. If this error persists, please contact customer services.</p>
    </div>
    <footer class="footer">
        <div>
            <ul class="nav nav-pills pull-right">
                <li class="presentation"><a href="http://help.sky.com">Help</a></li>
                <li class="presentation"><a href="http://contactus.sky.com">Contact</a></li>
            </ul>
        </div>
        <p>&copy; Sky 2015</p>
    </footer>
</div>
</body>
</html>