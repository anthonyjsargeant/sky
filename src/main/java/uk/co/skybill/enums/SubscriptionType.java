package uk.co.skybill.enums;

/**
 * Created by ant on 09/06/2015.
 */
public enum SubscriptionType {
    TV("tv"),
    TALK("talk"),
    BROADBAND("broadband");

    private String type;


    SubscriptionType(String type) {
        this.type = type;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }
}
