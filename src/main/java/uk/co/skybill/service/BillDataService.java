package uk.co.skybill.service;


import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * The Class BillDataService which is responsible for connecting to the remote bill service and obtaining
 * a customer's bill in raw JSON format.
 */
@Component
public class BillDataService {

    @Value("${bill.data.endpoint}")
    private String billDataEndpoint;

    @Autowired
    private HttpClientBuilder httpClientBuilder;

    /**
     * Gets the bill data.
     *
     * @return the bill data
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String getBillData() throws IOException {
        String billData;

        CloseableHttpClient httpClient = httpClientBuilder.build();

        HttpGet billDataRequest = new HttpGet(billDataEndpoint);
        CloseableHttpResponse response = httpClient.execute(billDataRequest);

        StatusLine statusLine = response.getStatusLine();

        if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
            billData = EntityUtils.toString(response.getEntity());
        } else if (statusLine.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
            throw new HttpResponseException(HttpStatus.SC_NOT_FOUND, "Bill data cannot be found.");
        } else if (statusLine.getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR) {
            throw new HttpResponseException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Bill service is not available.");
        } else {
            throw new HttpResponseException(statusLine.getStatusCode(), "Bill Service is not available due to an unexpected server response. " + statusLine.getReasonPhrase());
        }
        return billData;
    }


}
