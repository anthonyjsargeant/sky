package uk.co.skybill.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import uk.co.skybill.domain.Bill;
import uk.co.skybill.manager.BillManager;

/**
 * The Class SkyBillController which handles the http requests from the front end.
 */
@Controller
public class SkyBillController {
    private static final Logger LOG = LoggerFactory.getLogger(SkyBillController.class);

    @Autowired
    private BillManager billManager;

    /**
     * Gets and display the bill.
     *
     * @return the model and view
     */
    @RequestMapping("/currentBill")
    public ModelAndView displayBill() {
        LOG.debug("Entering controller...");

        ModelAndView modelAndView;

        Bill currentBill = billManager.getBillDataForCustomer(123456789L);

        if (currentBill == null) {
            modelAndView = new ModelAndView("billNotFound");
        } else {
            modelAndView = new ModelAndView("currentBill");
            modelAndView.addObject("currentBill", currentBill);
        }

        return modelAndView;
    }
}
