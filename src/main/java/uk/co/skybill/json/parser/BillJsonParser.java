package uk.co.skybill.json.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import uk.co.skybill.domain.Bill;

import java.io.IOException;

/**
 * The Class BillJsonParser which is responsible for the parsing of the raw JSON data into a Bill object.
 */
@Component
public class BillJsonParser {
    private static final Logger LOG = LoggerFactory.getLogger(BillJsonParser.class);

    /**
     * Generate bill from raw json.
     *
     * @param rawJsonData the raw json data
     * @return the bill
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Bill generateBillFromRawJson(String rawJsonData) throws IOException {

        byte[] jsonData = rawJsonData.getBytes();

        ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.readValue(jsonData, Bill.class);
    }
}
