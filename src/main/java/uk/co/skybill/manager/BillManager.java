package uk.co.skybill.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.co.skybill.domain.Bill;
import uk.co.skybill.json.parser.BillJsonParser;
import uk.co.skybill.service.BillDataService;

import java.io.IOException;

/**
 * The Class BillManager which is responsible for calling the BillDataService and then passing the raw JSON
 * onto the BillJsonParser.
 */
@Component
public class BillManager {

    private static final Logger LOG = LoggerFactory.getLogger(BillManager.class);

    @Autowired
    private BillDataService billDataService;

    @Autowired
    private BillJsonParser billJsonParser;

    /**
     * Gets the bill data for customer.
     *
     * @param customerId the customer id
     * @return the bill data for customer
     */
    public Bill getBillDataForCustomer(long customerId) {
        Bill bill = null;

        try {
            LOG.debug("Getting bill for customer ID: " + customerId);
            bill = billJsonParser.generateBillFromRawJson(billDataService.getBillData());
        } catch (IOException e) {
            LOG.warn("Unable to generate bill from bill data.", e);
        }

        return bill;
    }
}
