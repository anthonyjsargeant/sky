package uk.co.skybill.util;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * The Class BillDateFormatter which is responsible for taking the date format in the raw Bill JSON data
 * and formatting it to dd/MM/yyyy format..
 */
public class BillDateFormatter {

    /**
     * Gets the date in day month year format from yyyy-MM-dd format.
     *
     * @param date the date
     * @return the date in day month year format
     */
    public static String getDateInDayMonthYearFormat(String date) {
        String formattedDate = "";

        if (StringUtils.isNotBlank(date)) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime dateTime = formatter.parseDateTime(date);

            formattedDate = String.format("%02d/%02d/%d",
                    dateTime.getDayOfMonth(), dateTime.getMonthOfYear(), dateTime.getYear());
        }

        return formattedDate;
    }
}
