package uk.co.skybill.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import uk.co.skybill.beans.BuyAndKeep;
import uk.co.skybill.beans.Rental;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class SkyStore represents a collection of rentals and buy and keep purchases. Also holds the total charge cost.
 */
public class SkyStore {

    @JsonProperty("rentals")
    private List<Rental> rentals;

    @JsonProperty("buyAndKeep")
    private List<BuyAndKeep> buyAndKeep;

    @JsonProperty("total")
    private String totalCost;

    /**
     * Instantiates a new sky store object and initialises the fields.
     */
    public SkyStore() {
        rentals = new ArrayList<Rental>();
        buyAndKeep = new ArrayList<BuyAndKeep>();
        totalCost = "";
    }

    /**
     * Gets the rentals.
     *
     * @return the rentals
     */
    public List<Rental> getRentals() {
        return rentals;
    }

    /**
     * Sets the rentals.
     *
     * @param rentals the new rentals
     */
    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    /**
     * Gets the buy and keep.
     *
     * @return the buy and keep
     */
    public List<BuyAndKeep> getBuyAndKeep() {
        return buyAndKeep;
    }

    /**
     * Sets the buy and keep.
     *
     * @param buyAndKeep the new buy and keep
     */
    public void setBuyAndKeep(List<BuyAndKeep> buyAndKeep) {
        this.buyAndKeep = buyAndKeep;
    }

    /**
     * Gets the total cost.
     *
     * @return the total cost
     */
    public String getTotalCost() {
        if (rentals.isEmpty() && buyAndKeep.isEmpty()) {
            return "0.00";
        }
        return totalCost;
    }

    /**
     * Sets the total cost.
     *
     * @param totalCost the new total cost
     */
    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        SkyStore rhs = (SkyStore) obj;
        return new EqualsBuilder()
                .append(this.rentals, rhs.rentals)
                .append(this.buyAndKeep, rhs.buyAndKeep)
                .append(this.totalCost, rhs.totalCost)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(rentals)
                .append(buyAndKeep)
                .append(totalCost)
                .toHashCode();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("rentals", rentals)
                .append("buyAndKeep", buyAndKeep)
                .append("totalCost", totalCost)
                .toString();
    }
}
