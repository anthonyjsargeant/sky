package uk.co.skybill.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import uk.co.skybill.beans.Subscription;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class SkyPackage which holds the details of a customer's Sky Package.
 */
public class SkyPackage {

    @JsonProperty("subscriptions")
    private List<Subscription> charges;

    @JsonProperty("total")
    private String totalCost;

    /**
     * Instantiates a new sky package object and initialises the fields.
     */
    public SkyPackage() {
        charges = new ArrayList<Subscription>();
        totalCost = "";
    }

    /**
     * Gets the charges.
     *
     * @return the charges
     */
    public List<Subscription> getCharges() {
        return charges;
    }

    /**
     * Sets the charges.
     *
     * @param charges the new charges
     */
    public void setCharges(List<Subscription> charges) {
        this.charges = charges;
    }

    /**
     * Gets the total cost.
     *
     * @return the total cost
     */
    public String getTotalCost() {

        if (charges.isEmpty()) {
            return "0.00";
        }
        return totalCost;
    }

    /**
     * Sets the total cost.
     *
     * @param totalCost the new total cost
     */
    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        SkyPackage rhs = (SkyPackage) obj;
        return new EqualsBuilder()
                .append(this.charges, rhs.charges)
                .append(this.totalCost, rhs.totalCost)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(charges)
                .append(totalCost)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("charges", charges)
                .append("totalCost", totalCost)
                .toString();
    }
}
