package uk.co.skybill.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import uk.co.skybill.beans.Statement;

/**
 * The Class Bill which is the high-level object representing a Sky Bill.
 */
public class Bill {

    @JsonProperty("statement")
    private Statement statement;

    @JsonProperty("package")
    private SkyPackage skyPackageCharges;

    @JsonProperty("callCharges")
    private CallCharges callCharges;

    @JsonProperty("skyStore")
    private SkyStore skyStoreCharges;

    @JsonProperty("total")
    private String total;

    /**
     * Gets the statement.
     *
     * @return the statement
     */
    public Statement getStatement() {
        return statement;
    }

    /**
     * Sets the statement.
     *
     * @param statement the new statement
     */
    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    /**
     * Gets the sky package charges.
     *
     * @return the sky package charges
     */
    public SkyPackage getSkyPackageCharges() {
        return skyPackageCharges;
    }

    /**
     * Sets the sky package charges.
     *
     * @param skyPackageCharges the new sky package charges
     */
    public void setSkyPackageCharges(SkyPackage skyPackageCharges) {
        this.skyPackageCharges = skyPackageCharges;
    }

    /**
     * Gets the call charges.
     *
     * @return the call charges
     */
    public CallCharges getCallCharges() {
        return callCharges;
    }

    /**
     * Sets the call charges.
     *
     * @param callCharges the new call charges
     */
    public void setCallCharges(CallCharges callCharges) {
        this.callCharges = callCharges;
    }

    /**
     * Gets the sky store charges.
     *
     * @return the sky store charges
     */
    public SkyStore getSkyStoreCharges() {
        return skyStoreCharges;
    }

    /**
     * Sets the sky store charges.
     *
     * @param skyStoreCharges the new sky store charges
     */
    public void setSkyStoreCharges(SkyStore skyStoreCharges) {
        this.skyStoreCharges = skyStoreCharges;
    }

    /**
     * Gets the total cost of the bill.
     *
     * @return the total
     */
    public String getTotal() {
        return total;
    }

    /**
     * Sets the total cost of the bill.
     *
     * @param total the new total
     */
    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Bill rhs = (Bill) obj;
        return new EqualsBuilder()
                .append(this.statement, rhs.statement)
                .append(this.skyPackageCharges, rhs.skyPackageCharges)
                .append(this.callCharges, rhs.callCharges)
                .append(this.skyStoreCharges, rhs.skyStoreCharges)
                .append(this.total, rhs.total)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(statement)
                .append(skyPackageCharges)
                .append(callCharges)
                .append(skyStoreCharges)
                .append(total)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("statement", statement)
                .append("skyPackageCharges", skyPackageCharges)
                .append("callCharges", callCharges)
                .append("skyStoreCharges", skyStoreCharges)
                .append("total", total)
                .toString();
    }
}
