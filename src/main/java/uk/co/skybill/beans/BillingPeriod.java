package uk.co.skybill.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import uk.co.skybill.util.BillDateFormatter;

/**
 * The Class BillingPeriod which represents a billing date.
 */
public class BillingPeriod {

    @JsonProperty("from")
    private String fromDate;

    @JsonProperty("to")
    private String toDate;

    /**
     * Gets the from date pre-formatted as dd/MM/yyyy.
     *
     * @return the from date
     */
    public String getFromDate() {
        return StringUtils.isBlank(fromDate) ? "" : BillDateFormatter.getDateInDayMonthYearFormat(fromDate);
    }

    /**
     * Sets the from date.
     *
     * @param fromDate the new from date
     */
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * Gets the to date pre-formatted as dd/MM/yyyy.
     *
     * @return the to date
     */
    public String getToDate() {
        return StringUtils.isBlank(toDate) ? "" : BillDateFormatter.getDateInDayMonthYearFormat(toDate);
    }

    /**
     * Sets the to date.
     *
     * @param toDate the new to date
     */
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        BillingPeriod rhs = (BillingPeriod) obj;
        return new EqualsBuilder()
                .append(this.fromDate, rhs.fromDate)
                .append(this.toDate, rhs.toDate)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(fromDate)
                .append(toDate)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("fromDate", fromDate)
                .append("toDate", toDate)
                .toString();
    }
}
