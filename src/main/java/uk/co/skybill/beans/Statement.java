package uk.co.skybill.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import uk.co.skybill.util.BillDateFormatter;

/**
 * The Class Statement which represents the data relating to the Statement.
 */
public class Statement {

    @JsonProperty("generated")
    private String generatedDate;

    @JsonProperty("due")
    private String dueDate;

    @JsonProperty("period")
    private BillingPeriod billingPeriod;

    /**
     * Gets the generated date pre-formatted as dd/MM/yyyy.
     *
     * @return the generated date
     */
    public String getGeneratedDate() {
        return BillDateFormatter.getDateInDayMonthYearFormat(generatedDate);
    }

    /**
     * Sets the generated date.
     *
     * @param generatedDate the new generated date
     */
    public void setGeneratedDate(String generatedDate) {
        this.generatedDate = generatedDate;
    }

    /**
     * Gets the due date pre-formatted as dd/MM/yyyy.
     *
     * @return the due date
     */
    public String getDueDate() {
        return BillDateFormatter.getDateInDayMonthYearFormat(dueDate);
    }

    /**
     * Sets the due date.
     *
     * @param dueDate the new due date
     */
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Gets the billing period.
     *
     * @return the billing period
     */
    public BillingPeriod getBillingPeriod() {
        return billingPeriod;
    }

    /**
     * Sets the billing period.
     *
     * @param billingPeriod the new billing period
     */
    public void setBillingPeriod(BillingPeriod billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Statement rhs = (Statement) obj;
        return new EqualsBuilder()
                .append(this.generatedDate, rhs.generatedDate)
                .append(this.dueDate, rhs.dueDate)
                .append(this.billingPeriod, rhs.billingPeriod)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(generatedDate)
                .append(dueDate)
                .append(billingPeriod)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("generatedDate", generatedDate)
                .append("dueDate", dueDate)
                .append("billingPeriod", billingPeriod)
                .toString();
    }
}
