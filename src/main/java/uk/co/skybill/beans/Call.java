package uk.co.skybill.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * The Class Call which represents a phone call.
 */
public class Call extends Charge {

    @JsonProperty("called")
    private String numberCalled;

    @JsonProperty("duration")
    private String callDuration;

    /**
     * Gets the number called.
     *
     * @return the number called
     */
    public String getNumberCalled() {
        return numberCalled;
    }

    /**
     * Sets the number called.
     *
     * @param numberCalled the new number called
     */
    public void setNumberCalled(String numberCalled) {
        this.numberCalled = numberCalled;
    }

    /**
     * Gets the call duration.
     *
     * @return the call duration
     */
    public String getCallDuration() {
        return callDuration;
    }

    /**
     * Sets the call duration.
     *
     * @param callDuration the new call duration
     */
    public void setCallDuration(String callDuration) {
        this.callDuration = callDuration;
    }

    public String getCost() {
        return super.getCost();
    }

    public void setCost(String cost) {
        super.setCost(cost);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Call rhs = (Call) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.numberCalled, rhs.numberCalled)
                .append(this.callDuration, rhs.callDuration)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(numberCalled)
                .append(callDuration)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .appendSuper(super.toString())
                .append("numberCalled", numberCalled)
                .append("callDuration", callDuration)
                .toString();
    }
}
