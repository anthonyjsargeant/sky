package uk.co.skybill.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import uk.co.skybill.util.TestUtils;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
@WebAppConfiguration
public class SkyBillControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @InjectMocks
    private SkyBillController skyBillController;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void shouldReturn404ErrorCode() throws Exception {
        mockMvc.perform(get("/blahblahblah"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnModelAndViewWithBillObject() throws Exception {
        mockMvc.perform(get("/currentBill"))
                .andExpect(status().isOk())
                .andExpect(view().name("currentBill"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/currentBill.jsp"))
                .andExpect(model().attribute("currentBill", TestUtils.getBill()));
    }
}