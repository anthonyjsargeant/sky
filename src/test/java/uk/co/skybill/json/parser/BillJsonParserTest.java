package uk.co.skybill.json.parser;

import com.fasterxml.jackson.databind.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.skybill.util.TestUtils;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BillJsonParserTest {

    private static final Logger LOG = LoggerFactory.getLogger(BillJsonParserTest.class);

    private BillJsonParser billJsonParser;


    @Before
    public void setUp() throws Exception {
        billJsonParser = new BillJsonParser();
    }

    @Test(expected = JsonMappingException.class)
    public void shouldThrowJsonMappingExceptionIfBillDataIsEmpty() throws Exception {
        billJsonParser.generateBillFromRawJson("");
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionIfBillDataIsNull() throws Exception {
        billJsonParser.generateBillFromRawJson(null);
    }

    @Test
    public void shouldReturnSkyBillFromJsonData() throws Exception {
        assertThat(billJsonParser.generateBillFromRawJson(TestUtils.getRawJsonData()), is(TestUtils.getBill()));
    }
}