package uk.co.skybill.manager;

import org.apache.http.HttpStatus;
import org.apache.http.client.HttpResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.co.skybill.json.parser.BillJsonParser;
import uk.co.skybill.service.BillDataService;
import uk.co.skybill.util.TestUtils;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class BillManagerTest {

    @Mock
    private BillDataService billDataService;

    @Mock
    private BillJsonParser billJsonParser;

    @InjectMocks
    private BillManager manager;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void shouldReturnNullIfBillDataServiceReturnsInternalServerError() throws Exception {
        when(billDataService.getBillData())
                .thenThrow(new HttpResponseException(HttpStatus.SC_INTERNAL_SERVER_ERROR,
                        "Bill data service is not available."));

        assertThat(manager.getBillDataForCustomer(123456789L), is(nullValue()));
    }

    @Test
    public void shouldReturnNullIfBillDataServiceReturns404NotFound() throws Exception {
        when(billDataService.getBillData())
                .thenThrow(new HttpResponseException(HttpStatus.SC_NOT_FOUND,
                        "Bill data cannot be found."));

        assertThat(manager.getBillDataForCustomer(123456789L), is(nullValue()));
    }

    @Test
    public void shouldReturnNullIfBillDataServiceReturnsAnUnexpectedError() throws Exception {
        when(billDataService.getBillData())
                .thenThrow(new HttpResponseException(HttpStatus.SC_BAD_GATEWAY,
                        "Bill Service is not available due to an unexpected server response. Bad Gateway Test"));

        assertThat(manager.getBillDataForCustomer(123456789L), is(nullValue()));
    }

    @Test
    public void shouldReturnBillIfBillDataServiceReturnsRawBillData() throws Exception {
        when(billDataService.getBillData()).thenReturn(TestUtils.getRawJsonData());
        when(billJsonParser.generateBillFromRawJson(TestUtils.getRawJsonData())).thenReturn(TestUtils.getBill());

        assertThat(manager.getBillDataForCustomer(123456789L), is(TestUtils.getBill()));
    }
}
