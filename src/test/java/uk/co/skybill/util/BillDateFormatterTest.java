package uk.co.skybill.util;


import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BillDateFormatterTest {

    private BillDateFormatter formatter;

    @Before
    public void setUp() throws Exception {
        formatter = new BillDateFormatter();
    }

    @Test
    public void shouldReturnEmptyStringIfFromDateStringIsNull() throws Exception {
        assertThat(formatter.getDateInDayMonthYearFormat(null), is(""));
    }

    @Test
    public void shouldReturnEmptyStringIfToDateStringIsEmpty() throws Exception {
        assertThat(formatter.getDateInDayMonthYearFormat(""), is(""));
    }

    @Test
    public void shouldReturnFormattedDateStringInDayMonthYearFormat() throws Exception {
        assertThat(formatter.getDateInDayMonthYearFormat("2015-01-26"), is("26/01/2015"));
    }
}
