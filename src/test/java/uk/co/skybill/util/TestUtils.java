package uk.co.skybill.util;

import uk.co.skybill.beans.*;
import uk.co.skybill.domain.Bill;
import uk.co.skybill.domain.CallCharges;
import uk.co.skybill.domain.SkyPackage;
import uk.co.skybill.domain.SkyStore;
import uk.co.skybill.enums.SubscriptionType;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class TestUtils which holds all test objects for use in the Unit tests.
 */
public class TestUtils {

    public static final String VARIETY_WITH_MOVIES_COST = "50.00";
    public static final String SKY_TALK_ANYTIME_COST = "5.00";
    public static final String FIBRE_UNLIMITED_COST = "16.40";
    public static final String SINGLE_CALL_CHARGE_COST = "2.13";
    public static final String MOBILE_NUMBER = "07716393769";
    public static final String CALL_DURATION = "00:23:03";
    public static final String LAND_LINE_NUMBER = "02074351359";
    public static final String RENTAL_TITLE = "50 Shades of Grey";
    public static final String RENTAL_COST = "4.99";
    public static final String COMEDY_TITLE = "That's what she said";
    public static final String BUY_AND_KEEP_COST = "9.99";
    public static final String DRAMA_TITLE = "Broke back mountain";
    public static final String FULL_PACKAGE_COST = "71.40";
    public static final String SKY_STORE_TOTAL_COST = "24.97";
    public static final String CALL_CHARGES_TOTAL_COST = "59.64";
    public static final String BILL_TOTAL = "136.03";


    /**
     * Gets the full sky package.
     *
     * @return the full sky package
     */
    public static SkyPackage getFullSkyPackage() {
        Subscription varietyWithMovies = new Subscription();
        varietyWithMovies.setSubscriptionType(SubscriptionType.TV.getType());
        varietyWithMovies.setDescription("Variety with Movies HD");
        varietyWithMovies.setCost(VARIETY_WITH_MOVIES_COST);

        Subscription skyTalkAnytime = new Subscription();
        skyTalkAnytime.setSubscriptionType(SubscriptionType.TALK.getType());
        skyTalkAnytime.setDescription("Sky Talk Anytime");
        skyTalkAnytime.setCost(SKY_TALK_ANYTIME_COST);

        Subscription fibreUnlimited = new Subscription();
        fibreUnlimited.setSubscriptionType(SubscriptionType.BROADBAND.getType());
        fibreUnlimited.setDescription("Fibre Unlimited");
        fibreUnlimited.setCost(FIBRE_UNLIMITED_COST);

        List<Subscription> fullSubscription = new ArrayList<Subscription>();
        fullSubscription.add(varietyWithMovies);
        fullSubscription.add(skyTalkAnytime);
        fullSubscription.add(fibreUnlimited);

        SkyPackage fullSkyPackage = new SkyPackage();
        fullSkyPackage.setCharges(fullSubscription);
        fullSkyPackage.setTotalCost(TestUtils.FULL_PACKAGE_COST);

        return fullSkyPackage;
    }

    /**
     * Gets the bill.
     *
     * @return the bill
     */
    public static Bill getBill() {
        Bill bill = new Bill();
        bill.setStatement(statementFor(billingPeriod()));
        bill.setCallCharges(getCallCharges());
        bill.setSkyPackageCharges(getFullSkyPackage());
        bill.setSkyStoreCharges(getSkyStoreCharges());
        bill.setTotal(BILL_TOTAL);

        return bill;
    }

    /**
     * Statement for.
     *
     * @param billingPeriod the billing period
     * @return the statement
     */
    public static Statement statementFor(BillingPeriod billingPeriod) {
        Statement statement = new Statement();
        statement.setGeneratedDate("2015-01-11");
        statement.setDueDate("2015-01-25");
        statement.setBillingPeriod(billingPeriod);

        return statement;
    }

    /**
     * Billing period.
     *
     * @return the billing period
     */
    public static BillingPeriod billingPeriod() {
        BillingPeriod billingPeriod = new BillingPeriod();
        billingPeriod.setFromDate("2015-01-26");
        billingPeriod.setToDate("2015-02-25");

        return billingPeriod;
    }

    /**
     * Gets the call charges.
     *
     * @return the call charges
     */
    public static CallCharges getCallCharges() {
        Call mobileCall = new Call();
        mobileCall.setNumberCalled(MOBILE_NUMBER);
        mobileCall.setCallDuration("00:23:03");
        mobileCall.setCost(SINGLE_CALL_CHARGE_COST);

        Call landLineCall = new Call();
        landLineCall.setNumberCalled(LAND_LINE_NUMBER);
        landLineCall.setCallDuration(CALL_DURATION);
        landLineCall.setCost(SINGLE_CALL_CHARGE_COST);

        List<Call> calls = new ArrayList<Call>();
        for (int i = 0; i < 18; i++) {
            calls.add(mobileCall);
        }

        for (int i = 0; i < 10; i++) {
            calls.add(landLineCall);
        }

        CallCharges callCharges = new CallCharges();
        callCharges.setCharges(calls);
        callCharges.setTotalCost(CALL_CHARGES_TOTAL_COST);

        return callCharges;
    }

    /**
     * Gets the sky store charges.
     *
     * @return the sky store charges
     */
    public static SkyStore getSkyStoreCharges() {
        Rental rental = new Rental();
        rental.setTitle(RENTAL_TITLE);
        rental.setCost(RENTAL_COST);

        BuyAndKeep comedy = new BuyAndKeep();
        comedy.setTitle(COMEDY_TITLE);
        comedy.setCost(BUY_AND_KEEP_COST);

        BuyAndKeep drama = new BuyAndKeep();
        drama.setTitle(DRAMA_TITLE);
        drama.setCost(BUY_AND_KEEP_COST);

        List<Rental> skyStoreRentals = new ArrayList<Rental>();
        skyStoreRentals.add(rental);

        List<BuyAndKeep> skyStoreBuyAndKeep = new ArrayList<BuyAndKeep>();
        skyStoreBuyAndKeep.add(comedy);
        skyStoreBuyAndKeep.add(drama);

        SkyStore skyStore = new SkyStore();
        skyStore.setRentals(skyStoreRentals);
        skyStore.setBuyAndKeep(skyStoreBuyAndKeep);
        skyStore.setTotalCost(SKY_STORE_TOTAL_COST);

        return skyStore;
    }

    /**
     * Gets the raw json data.
     *
     * @return the raw json data
     */
    public static String getRawJsonData() {
        return "{\n" +
                "  \"statement\": {\n" +
                "    \"generated\": \"2015-01-11\",\n" +
                "    \"due\": \"2015-01-25\",\n" +
                "    \"period\": {\n" +
                "      \"from\": \"2015-01-26\",\n" +
                "      \"to\": \"2015-02-25\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"total\": 136.03,\n" +
                "  \"package\": {\n" +
                "    \"subscriptions\": [\n" +
                "      { \"type\": \"tv\", \"name\": \"Variety with Movies HD\", \"cost\": 50.00 },\n" +
                "      { \"type\": \"talk\", \"name\": \"Sky Talk Anytime\", \"cost\": 5.00 },\n" +
                "      { \"type\": \"broadband\", \"name\": \"Fibre Unlimited\", \"cost\": 16.40 }\n" +
                "    ],\n" +
                "    \"total\": 71.40\n" +
                "  },\n" +
                "  \"callCharges\": {\n" +
                "    \"calls\": [\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"07716393769\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 },\n" +
                "      { \"called\": \"02074351359\", \"duration\": \"00:23:03\", \"cost\": 2.13 }\n" +
                "    ],\n" +
                "    \"total\": 59.64\n" +
                "  },\n" +
                "  \"skyStore\": {\n" +
                "    \"rentals\": [\n" +
                "      { \"title\": \"50 Shades of Grey\", \"cost\": 4.99 }\n" +
                "    ],\n" +
                "    \"buyAndKeep\": [\n" +
                "      { \"title\": \"That's what she said\", \"cost\": 9.99 },\n" +
                "      { \"title\": \"Broke back mountain\", \"cost\": 9.99 }\n" +
                "    ],\n" +
                "    \"total\": 24.97\n" +
                "  }\n" +
                "}";
    }
}
