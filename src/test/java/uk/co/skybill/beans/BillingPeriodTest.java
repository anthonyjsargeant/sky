package uk.co.skybill.beans;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


/**
 * The Class BillingPeriodTest.
 */
public class BillingPeriodTest {

    private BillingPeriod billingPeriod;

    /**
     * Sets the up the test fixtures.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        billingPeriod = new BillingPeriod();
    }

    /**
     * Should return empty string if from date string is null or empty.
     *
     * @throws Exception if anything should go wrong.
     */
    @Test
    public void shouldReturnEmptyStringIfFromDateStringIsNullOrEmpty() throws Exception {
        assertThat(billingPeriod.getFromDate(), is(""));
    }

    /**
     * Should return empty string if to date string is null or empty.
     *
     * @throws Exception if anything should go wrong.
     */
    @Test
    public void shouldReturnEmptyStringIfToDateStringIsNullOrEmpty() throws Exception {
        assertThat(billingPeriod.getToDate(), is(""));
    }

    /**
     * Should return from date string in day month year format.
     *
     * @throws Exception if anything should go wrong.
     */
    @Test
    public void shouldReturnFromDateStringInDayMonthYearFormat() throws Exception {
        billingPeriod.setFromDate("2015-01-26");
        assertThat(billingPeriod.getFromDate(), is("26/01/2015"));
    }

    /**
     * Should return to date string in day month year format.
     *
     * @throws Exception if anything should go wrong.
     */
    @Test
    public void shouldReturnToDateStringInDayMonthYearFormat() throws Exception {
        billingPeriod.setFromDate("2015-02-25");
        assertThat(billingPeriod.getFromDate(), is("25/02/2015"));
    }
}