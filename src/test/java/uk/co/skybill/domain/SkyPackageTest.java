package uk.co.skybill.domain;

import org.junit.Before;
import org.junit.Test;
import uk.co.skybill.beans.Subscription;
import uk.co.skybill.enums.SubscriptionType;
import uk.co.skybill.util.TestUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class SkyPackageTest {

    private SkyPackage emptySkyPackage;
    private SkyPackage singleSubscriptionSkyPackage;
    private SkyPackage fullSkyPackage;

    @Before
    public void setUp() {

        Subscription varietyWithMovies = new Subscription();
        varietyWithMovies.setSubscriptionType(SubscriptionType.TV.getType());
        varietyWithMovies.setDescription("Variety with Movies HD");
        varietyWithMovies.setCost(TestUtils.VARIETY_WITH_MOVIES_COST);

        Subscription skyTalkAnytime = new Subscription();
        skyTalkAnytime.setSubscriptionType(SubscriptionType.TALK.getType());
        skyTalkAnytime.setDescription("Sky Talk Anytime");
        skyTalkAnytime.setCost(TestUtils.SKY_TALK_ANYTIME_COST);

        Subscription fibreUnlimited = new Subscription();
        fibreUnlimited.setSubscriptionType(SubscriptionType.BROADBAND.getType());
        fibreUnlimited.setDescription("Fibre Unlimited");
        fibreUnlimited.setCost(TestUtils.FIBRE_UNLIMITED_COST);

        List<Subscription> singleSubscription = new ArrayList<Subscription>();
        singleSubscription.add(varietyWithMovies);

        List<Subscription> fullSubscription = new ArrayList<Subscription>();
        fullSubscription.add(varietyWithMovies);
        fullSubscription.add(skyTalkAnytime);
        fullSubscription.add(fibreUnlimited);

        emptySkyPackage = new SkyPackage();

        singleSubscriptionSkyPackage = new SkyPackage();
        singleSubscriptionSkyPackage.setCharges(singleSubscription);
        singleSubscriptionSkyPackage.setTotalCost(TestUtils.VARIETY_WITH_MOVIES_COST);

        fullSkyPackage = TestUtils.getFullSkyPackage();
    }

    @Test
    public void shouldReturnZeroIfPackageHasNoSubscriptions() throws Exception {
        assertThat(emptySkyPackage.getTotalCost(), is("0.00"));
    }

    @Test
    public void shouldReturnCostOfASingleSubscription() throws Exception {
        assertThat(singleSubscriptionSkyPackage.getTotalCost(), is(TestUtils.VARIETY_WITH_MOVIES_COST));
    }

    @Test
    public void shouldReturnCostOfAFullPackage() throws Exception {
        assertThat(fullSkyPackage.getTotalCost(), is(TestUtils.FULL_PACKAGE_COST));
    }
}