package uk.co.skybill.domain;

import org.junit.Before;
import org.junit.Test;
import uk.co.skybill.beans.Call;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class CallChargesTest {

    private static final String SINGLE_CALL_CHARGE_COST = "2.13";

    private Call aCall;

    private CallCharges zeroCallCharges;
    private CallCharges singleCallCharges;
    private CallCharges multipleCallCharges;

    private List<Call> singleCall;
    private List<Call> tenCalls;

    @Before
    public void setUp() throws Exception {
        zeroCallCharges = new CallCharges();

        aCall = new Call();
        aCall.setNumberCalled("07716393769");
        aCall.setCallDuration("00:23:03");
        aCall.setCost(SINGLE_CALL_CHARGE_COST);

        singleCall = new ArrayList<Call>();
        singleCall.add(aCall);

        tenCalls = new ArrayList<Call>();
        for (int i = 0; i < 10; i++) {
            tenCalls.add(aCall);
        }

        singleCallCharges = new CallCharges();
        singleCallCharges.setCharges(singleCall);
        singleCallCharges.setTotalCost(SINGLE_CALL_CHARGE_COST);

        multipleCallCharges = new CallCharges();
        multipleCallCharges.setCharges(tenCalls);
        multipleCallCharges.setTotalCost("59.64");

    }

    @Test
    public void shouldReturnZeroIfThereAreNoCallCharges() throws Exception {
        assertThat(zeroCallCharges.getTotalCost(), is("0.00"));
    }

    @Test
    public void shouldReturnCostOfASingleCallCharge() throws Exception {
        assertThat(singleCallCharges.getTotalCost(), is(SINGLE_CALL_CHARGE_COST));
    }

    @Test
    public void shouldReturnCostOfAMultipleCallCharges() throws Exception {
        assertThat(multipleCallCharges.getTotalCost(), is("59.64"));
    }
}