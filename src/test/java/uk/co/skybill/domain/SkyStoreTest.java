package uk.co.skybill.domain;

import org.junit.Before;
import org.junit.Test;
import uk.co.skybill.beans.BuyAndKeep;
import uk.co.skybill.beans.Rental;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class SkyStoreTest {

    private SkyStore noRentalsAndNoBuyAndKeeps;
    private SkyStore oneRentalOnly;
    private SkyStore oneBuyAndKeepOnly;
    private SkyStore oneRentalAndOneBuyAndKeep;
    private SkyStore combinationOfRentalAndBuyAndKeeps;

    private Rental rental;

    private BuyAndKeep comedy;
    private BuyAndKeep drama;

    private List<Rental> oneRental;
    private List<BuyAndKeep> oneBuyAndKeep;
    private List<BuyAndKeep> twoBuyAndKeep;

    @Before
    public void setUp() throws Exception {
        rental = new Rental();
        rental.setTitle("50 Shades of Grey");
        rental.setCost("4.99");

        comedy = new BuyAndKeep();
        comedy.setTitle("That's what she said");
        comedy.setCost("9.99");

        drama = new BuyAndKeep();
        drama.setTitle("Broke back mountain");
        drama.setCost("9.99");

        oneRental = new ArrayList<Rental>();
        oneRental.add(rental);

        oneBuyAndKeep = new ArrayList<BuyAndKeep>();
        oneBuyAndKeep.add(drama);

        twoBuyAndKeep = new ArrayList<BuyAndKeep>();
        twoBuyAndKeep.add(comedy);
        twoBuyAndKeep.add(drama);

        noRentalsAndNoBuyAndKeeps = new SkyStore();

        oneRentalOnly = new SkyStore();
        oneRentalOnly.setRentals(oneRental);
        oneRentalOnly.setTotalCost(rental.getCost());

        oneBuyAndKeepOnly = new SkyStore();
        oneBuyAndKeepOnly.setBuyAndKeep(oneBuyAndKeep);
        oneBuyAndKeepOnly.setTotalCost(drama.getCost());

        oneRentalAndOneBuyAndKeep = new SkyStore();
        oneRentalAndOneBuyAndKeep.setRentals(oneRental);
        oneRentalAndOneBuyAndKeep.setBuyAndKeep(oneBuyAndKeep);
        oneRentalAndOneBuyAndKeep.setTotalCost(rental.getCost());
        oneRentalAndOneBuyAndKeep.setTotalCost("14.98");

        combinationOfRentalAndBuyAndKeeps = new SkyStore();
        combinationOfRentalAndBuyAndKeeps.setRentals(oneRental);
        combinationOfRentalAndBuyAndKeeps.setBuyAndKeep(twoBuyAndKeep);
        combinationOfRentalAndBuyAndKeeps.setTotalCost("24.97");
    }

    @Test
    public void shouldReturnZeroIfPackageHasNoRentalsAndNoBuyAndKeeps() throws Exception {
        assertThat(noRentalsAndNoBuyAndKeeps.getTotalCost(), is("0.00"));
    }

    @Test
    public void shouldReturnCostOfASingleRental() throws Exception {
        assertThat(oneRentalOnly.getTotalCost(), is(rental.getCost()));
    }

    @Test
    public void shouldReturnCostOfASingleBuyAndKeep() throws Exception {
        assertThat(oneBuyAndKeepOnly.getTotalCost(), is(drama.getCost()));
    }

    @Test
    public void shouldReturnCostOfARentalAndABuyAndKeep() throws Exception {
        assertThat(oneRentalAndOneBuyAndKeep.getTotalCost(), is("14.98"));
    }

    @Test
    public void shouldReturnCostOfACombinationOfRentalsAndBuyAndKeeps() throws Exception {
        assertThat(combinationOfRentalAndBuyAndKeeps.getTotalCost(), is("24.97"));
    }
}