package uk.co.skybill.service;

import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import uk.co.skybill.util.TestUtils;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class BillDataServiceTest {

    @Value("${bill.data.endpoint}")
    private String billDataEndpoint;

    @Mock
    private CloseableHttpClient httpClient;

    @Mock
    private HttpClientBuilder httpClientBuilder;

    @Mock
    private CloseableHttpResponse response;

    @Mock
    private StatusLine statusLine;

    @InjectMocks
    private BillDataService billDataService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        ReflectionTestUtils.setField(billDataService, "billDataEndpoint", billDataEndpoint);

        when(httpClientBuilder.build()).thenReturn(httpClient);
        when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
        when(response.getStatusLine()).thenReturn(statusLine);
    }

    @Test
    public void shouldThrowExceptionIfBillDataIsNotFound() throws Exception {
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_NOT_FOUND);

        thrown.expect(HttpResponseException.class);
        thrown.expectMessage(is("Bill data cannot be found."));

        billDataService.getBillData();
    }

    @Test
    public void shouldThrowExceptionIfThereIsAServerError() throws Exception {
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_INTERNAL_SERVER_ERROR);

        thrown.expect(HttpResponseException.class);
        thrown.expectMessage(is("Bill service is not available."));

        billDataService.getBillData();
    }

    @Test
    public void shouldThrowsExceptionIfThereIsAnUnexpectedError() throws Exception {
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_BAD_GATEWAY);
        when(statusLine.getReasonPhrase()).thenReturn("Bad Gateway Test");

        thrown.expect(HttpResponseException.class);
        thrown.expectMessage(is("Bill Service is not available due to an unexpected server response. Bad Gateway Test"));

        billDataService.getBillData();
    }

    @Test
    public void shouldReturnBillData() throws Exception {
        StringEntity entity = new StringEntity(TestUtils.getRawJsonData());

        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
        when(response.getEntity()).thenReturn(entity);

        assertThat(billDataService.getBillData(), is(TestUtils.getRawJsonData()));
    }
}
