# README #

System Requirements:
 - Java 7
 - Apache Maven

In order to run the SkyBillApp, simply clone this repo and run the following maven command from the command line:

$ mvn clean install tomcat7:run

Once running, you can see the site by visiting:

http://localhost:8080/skybillapp